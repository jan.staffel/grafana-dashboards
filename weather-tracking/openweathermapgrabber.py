import http.client
import json
import redis
import datetime
import socket
import sys
import math
import time
import requests
from redistimeseries.client import Client

r = redis.Redis()
now = int(time.time())

rts = Client()

CONST_ZIPCODE=''
CONST_COUNTRYCODE=''
CONST_UNITS='metric'
CONST_APIKEY=''

try:
	requestPayload = {
		'zip': CONST_ZIPCODE+','+CONST_COUNTRYCODE,
		'units': CONST_UNITS,
		'appid': CONST_APIKEY
	}
	response = requests.get('https://api.openweathermap.org/data/2.5/weather', params=requestPayload)
	response.raise_for_status()
	data = response.json()
except socket.timeout as st:
	print(st)
	sys.exit(1)
except ConnectionError as e:
	print(e)
	sys.exit(1)
except HTTPError as e:
	print(e)
	sys.exit(1)
except ValueError as e:
	print(e)
	sys.exit(1)
except requests.exceptions.RequestException as e:
	print(e)
	sys.exit(1)

dt = data.get('dt', now)*1000

weather = {
#	'weather': (0, data['weather'][0]['main'])['weather' in data and 'main' in data.get('weather')[0]],
#	'weather_description': (0, data['weather'][0]['description'])['weather' in data and 'description' in data.get('weather')[0]],
	'temperature': (0, data['main'].get('temp'),0)['main' in data],
	'temperature_feels': (0, data['main'].get('feels_like'),0)['main' in data],
	'temperature_min': (0, data['main'].get('temp_min'),0)['main' in data],
	'temperature_max': (0, data['main'].get('temp_max'),0)['main' in data],
	'pressure': (0, data['main'].get('pressure'),0)['main' in data],
	'humidity': (0, data['main'].get('humidity'),0)['main' in data],
	'wind_speed': (0, data['wind'].get('speed'),0)['wind' in data],
	'wind_direction': (0, data['wind'].get('deg'),0)['wind' in data],
	'wind_gust': (0, data['wind'].get('gust'),0)['wind' in data],
	'cloud_density': (0, data.get('clouds',{}).get('all',0))['clouds' in data],
	'rain_1h': (0, data.get('rain',{}).get('1h',0))['rain' in data],
#	'rain_3h': (0, data['rain'].get('3h'),0)['rain' in data],
#	'snow_1h': (0, data['snow'].get('1h'),0)['snow' in data],
#	'snow_3h': (0, data['snow'].get('3h'),0)['snow' in data],
	'datetime': (data.get('dt',0))
}

with r.pipeline() as pipe:
	for k,v in weather.items():
		try:
			if k == 'datetime':
				break
#   Creation function only has to be executed on first execution, or from withhin redis. Its left in here for documentation.
#			rts.create('weatherJuelich-'+k, labels={'Time':'Series','City':'Juelich','Data':k},duplicate_policy='last')
			rts.add('weatherJuelich-'+k, dt, v)
		except Exception as e:
			print(k)
			print(dt)
			print(e)
			continue
pipe.execute()
sys.exit(0)