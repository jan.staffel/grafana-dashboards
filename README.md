# grafana-dashboards

This project contains some useful dashboards for grafana. To import a dashboard, choose "Create/Import" in your grafana instance. There you can directly upload the JSON's you find here, or put the JSON's content into the import field.

Please be aware that you have to define your data sources independently. Different datasources require a rework of each dashboard metric.

## Power metering for Shelly

The power metering dashboard is built for a Shelly data structure. Data is collected using the built-in MQTT messaging and collected into an InfluxDB via Telegraf. Other Datasources can be added as needed.

### General setup

The Dashboard collects the available Shellys via a Database Query. It formats the available topics using a regex and puts the resulting names and topics into a variable. This allows to use Grafanas "Repeat" feature. Thusly, the measurements for all available shellys are automatically generated and can be shown by selecting or de-selecting them. The measurements are sorted into one row per metric.

The dashboard contains following rows:
- Power consumption: Shows the power consumption in Watts for a data point. Also shows the moving average. Please be aware that due to the nature of moving averages, it is running behind the time it represents. In this setup it averages the last 5 ticks.
- Energy consumption: Calculates the total energy consumption for the given timeframe.
- Voltage/Current: Shows the current and voltage
- Energy consumption since last switchoff: Shows the value of the "aenergy" field from Shellys data. To be honest, i'm not sure what since represent. I think this is the total energy consumption since the Shelly was last disconnected from the grid, but the values seem off.

### MQTT Data

Shellys Data structure is documented here: https://shelly-api-docs.shelly.cloud/gen2/Components/FunctionalComponents/Switch

The Mqtt setup is documented here: https://shelly-api-docs.shelly.cloud/gen2/Components/SystemComponents/Mqtt

In order for the auto-detect feature to work, each Shellys' topic needs to be set to a describing name, for example "desk" or "garage".

### Telegraf SystemD Service Unit and Telegraf config

Setting up InfluxDB and Telegraf or configuring SystemD is out of scope for this project. But setting up telegraf for this project requires to create one config file as well as one SystemD Unit per physical shelly device. An example for both is given within the dashboard folder.

You can directly take the Unit and the telegraf config. Please be aware of the following changes that have to be made to the examples:
- SystemD Unit File Name and Folder: The file name of the SystemD Unit is the name of the SystemD Service. I choose to name it after the shelly it represents. The path they go into may depend on your distro, in case of Debian and Ubuntu it is /etc/systemd/system/

For the telegraf configs, following changes have to be made:
- In the [agents] section, select the logfile you want to use for this config
- In the [[outputs.influxdb]] section, add the connection details for your InfluxDB and the database you want to use within it
- In the [[inputs.mqtt_consumer]] section, add the connection details for your MQTT broker. Also add the correct topic. It will be "<shelly-mqtt-topic>>/status/switch:0". More sophisticated Shellys might have more switches, which will require one config each.

##Weather Tracking from Openweathermap API

### General Setup

Data is being collected from the API provided by https://www.openweathermap.com. It uses the call for zipcodes as documented in https://openweathermap.org/current#zip to grab the data.
The Dashboard is built to use redis and its timeseriesplugin as data backend. This will be changed in the future, as is found redis to lack capabilities when it comes to time series handling.

The Dashboard contains the following features:

- Weather indicators for the most recent values. The ranges have been pre-defined to some senseful values.
- Graphical representation of historic values of Temperature, Rain, Humidity, Air Pressure, Wind speed and Cloud density

### Installation

The data grabber consists of a python script that does the actual work, and a systemd service file that executes the script every 10 minutes.
10 minutes has been chosen as this is the same granularity which Openweathermap uses to update the data.

In order to user the Openweathermap API, you need to register with them and generate an API key.
Also, you need an installation of Redis and the Timeseries Plugin at https://redis.io/docs/stack/timeseries/ .

The redis table has to be setup accordingly. An example on how to do this is in the openweathermapgrabber.py file.
Also make sure that redis is configured properly as a datasource in grafana.

For installation, place the opwenweathermapgrabber.py wherever you like. Fill the variables prefixed with CONST_<VALUE> with the values of your choice.
Afterwards you can place the openweathermapgrabber.service in your systemd location. Remember to replace the placeholder in the unit configuration, which are USER and the path to the grabber file.
Just daemon-reload, enable and start the service afterwards.